﻿using Microsoft.Win32;
using OfficeOpenXml;
using Renci.SshNet;
using System;
using System.Windows;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Threading;




namespace WpfApp1
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BTN_Import_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "选择数据表";
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "Excel文件|*.xlsx";
            if (!(bool)openFileDialog.ShowDialog())
            {
                return; // 被点了取消
            }
            var filename = openFileDialog.FileName;
            TB_FilePath.Text = filename;   // 返回excel文件路径到TB_1
            ExcelTo(filename);  // 打开Excel文件获取数据
            CB_Monitor.IsEnabled = true;
        }

        private int ExcelTo(string filePath)
        {
            // 创建一个新的ExcelPackage对象
            using (ExcelPackage package = new ExcelPackage(new FileInfo(filePath)))
            {
                // 获取第一个工作表
                ExcelWorksheet worksheet = package.Workbook.Worksheets[0];

                // 获取Excel中的行数和列数
                int rowCount = worksheet.Dimension.Rows;
                int columnCount = worksheet.Dimension.Columns;
               // 新建list对象
                List<Info> items = new List<Info>();
                // 遍历每一行
                for (int row = 1; row <= rowCount; row++)
                {
                    var ip = worksheet.Cells[row, 1].Value;     // IP
                    var port = worksheet.Cells[row, 2].Value;   // PORT
                    var user = worksheet.Cells[row, 3].Value;   // USER
                    var passwd = worksheet.Cells[row, 4].Value; // PASSWD
                    // 将数据导入列表
                    items.Add(new Info { IP = ip.ToString(), Port = port.ToString(), User = user.ToString(), Password = passwd.ToString() });
                }
                // 列表数据绑定
                list_1.ItemsSource = items;
            }
            return 1;
        }

        // 定义Info数据模型
        public class Info
        {
            public string IP { get; set; }
            public string Port { get; set; }
            public string User { get; set; }
            public string Password { get; set; }
        }

        // SSH连接功能模块
        private string SSHConn(string ip, int port, string user, string passwd, string comand)
        {
            // 新建client
            SshClient sshClient = new SshClient(ip, port, user, passwd);
            try
            {
                // 连接
                sshClient.Connect();
                // 执行命令
                SshCommand command = sshClient.CreateCommand(comand);
                // 获取返回值
                return command.Execute();
            }
            catch (Exception ex)
            {
                // 异常处理  
                //Console.WriteLine("连接或执行命令时发生错误: " + ex.Message);
                return ex.Message;
            }
            finally
            {
                // 关闭连接
                sshClient.Disconnect();
            }
        }

        // 获取服务器初始化数据
        private string[] InitSSHConn(string ip, int port, string user, string passwd, string comand)
        {
            // 初始数据
            string command_cpu = """top -bn1 | grep 'Cpu(s)' | grep -Eo '[0-9]+\.[0-9] id' | grep -Eo '[0-9]+\.[0-9]'""";
            string command_memUsed = """top -bn1 | grep 'KiB Mem' | grep -Eo '[0-9]+ used' | grep -Eo '[0-9]+'""";
            string command_memTotal = """top -bn1 | grep 'KiB Mem' | grep -Eo '([0-9]+ total)' | grep -Eo '[0-9]+'""";
            /* string command_diskVda2 = """df -h | grep -E '(/dev/vda2)' | grep -Eo '[0-9]+[0-9]%' | grep -Eo '[0-9]+[0-9]'""";
             string command_diskLv1 = """df -h | grep -E '(/dev/mapper/masdata-lv1)' | grep -Eo '[0-9]+%' | grep -Eo '[0-9]+'""";
             string command_diskLv01 = """df -h | grep -E '(/dev/mapper/cuiotvg-cuiotlv01)' | grep -Eo '[0-9]+%' | grep -Eo '[0-9]+'""";*/
            string command_diskPath = """df -h | grep '^/dev' | awk '{print $6}'""";        // 磁盘挂载目录 
            string command_diskVar = """df -h | grep '^/dev' | awk '{print $5}'""";         // 磁盘占比
            string command_os = """hostnamectl | sed -n '7p' | grep -Eo '(: [a-Z]+)' | grep -Eo '[a-Z]+'""";
            string ufw_state = "";
            string DISK_gen_size = "";
            string DISK_data_size = "";
            // 新建client
            SshClient sshClient = new SshClient(ip, port, user, passwd);
            try
            {
                // 连接
                sshClient.Connect();
                if (CB_1.IsChecked == false)    // 判断是否勾选“显示服务器性能”
                {
                    // 执行命令
                    SshCommand command = sshClient.CreateCommand(comand);
                    // 获取返回值
                    return new string[] { command.Execute() };
                }
                if (CB_1.IsChecked == true)
                {
                    // 执行命令
                    SshCommand CPU = sshClient.CreateCommand(command_cpu);
                    SshCommand MEMused = sshClient.CreateCommand(command_memUsed);
                    SshCommand MEMtotal = sshClient.CreateCommand(command_memTotal);
                    SshCommand DISKpath = sshClient.CreateCommand(command_diskPath);
                    SshCommand DISKvar = sshClient.CreateCommand(command_diskVar);
                    if (DISKpath.Execute().Trim().Split("\n")[0] == "/")    // 判断根目录
                    {
                        string str = DISKvar.Execute().Trim().Split("\n")[0];   // 分割SSH返回值
                        DISK_gen_size = str.Remove(str.Length - 1);    // 获取根目录占用
                        str = DISKvar.Execute().Trim().Split("\n")[1];
                        DISK_data_size = str.Remove(str.Length - 1);   // 获取数据盘占用
                    }
                    else if (DISKpath.Execute().Trim().Split("\n")[1] == "/")
                    {
                        string str = DISKvar.Execute().Trim().Split("\n")[1];   // 分割SSH返回值
                        DISK_gen_size = str.Remove(str.Length - 1);    // 获取根目录占用
                        str = DISKvar.Execute().Trim().Split("\n")[0];
                        DISK_data_size = str.Remove(str.Length - 1);   // 获取数据盘占用
                    }
                    if (CB_ufw.IsChecked == true)      // 判断是否勾选防火墙状态
                    {
                        SshCommand osType = sshClient.CreateCommand(command_os);
                        if (osType.Execute().Trim() == "CentOS")   // 判断服务器系统类型
                        {
                            SshCommand UFW_state = sshClient.CreateCommand("firewall-cmd --state");     // 查询防火墙状态
                            if (UFW_state.Execute().Trim() == "running")   // 判断防火墙是否运行
                            {
                                ufw_state = "running";
                            }
                        }
                        else if (osType.Execute().Trim() == "Ubuntu")
                        {
                            SshCommand UFW_state = sshClient.CreateCommand("""ufw status | grep -Eo '(: [a-Z]+)' | grep -Eo '[a-Z]+'""");
                            if (UFW_state.Execute().Trim() == "active")
                            {
                                ufw_state = "running";
                            }
                        }
                    }
                    // 获取返回值
                    return new string[] { CPU.Execute(), MEMused.Execute(), MEMtotal.Execute(), DISK_gen_size, DISK_data_size, ufw_state };
                }
                return null;
            }
            catch (Exception ex)
            {
                // 异常处理  
                //Console.WriteLine("连接或执行命令时发生错误: " + ex.Message);
                MessageBox.Show("SSH连接发生错误:" + ip + ex.Message);
                return new string[] { ex.Message };
            }
            finally
            {
                // 关闭连接
                sshClient.Disconnect();
            }
        }

        // SSH连接服务器并查询信息
        private void list_1_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // 获取双击行的数据
            List<Info> ls = new List<Info>(list_1.SelectedItems.Cast<Info>().ToList());
            // 遍历双击行的数据
            foreach (Info item in ls)
            {
                // 创建SSH连接并执行命令
                string coomand = TB_Command.Text;
                string[] outp = InitSSHConn(item.IP, Convert.ToInt16(item.Port), item.User, item.Password, coomand);
                if (CB_1.IsChecked == false)
                {
                    // 将IP返回至TB_2
                    TB_2.Text += item.IP.ToString() + "\n";
                    TB_2.Text += outp[0];
                }
                if (CB_1.IsChecked == true)    // 判断是否勾选“服务器性能”
                {
                    double cpu = Convert.ToDouble(outp[0]); // cpu空闲值
                    double mem = (Convert.ToDouble(outp[1]) / Convert.ToDouble(outp[2])) * 100;  // 内存占用
                    double diskGen = Convert.ToDouble(outp[3]);    // 根目录占用
                    double diskData = Convert.ToDouble(outp[4]);   // 数据盘占用
                    TB_2.Text = "防火墙状态:" + outp[5];     // 防火墙状态
                    ServersInfo(cpu, mem, diskGen, diskData);  // 数据传给前端
                }
            }
        }

        // 展示服务器性能占比
        private void ServersInfo(double cpuinfo, double meminfo, double diskGeninfo, double diskDatainfo)
        {
            int WN = Convert.ToInt16(TB_WN.Text);   // 获取预警值
            // CPU
            progressBar_cpu.Value = 100 - cpuinfo;
            progressBar_cpu.ToolTip = "CPU占用" + progressBar_cpu.Value + "%";
            if (progressBar_cpu.Value > WN) // 判断值
            {
                progressBar_cpu.Foreground = new SolidColorBrush(Colors.Red);   // 将进度条前景色改为红色
            }
            else
            {
                progressBar_cpu.Foreground = new SolidColorBrush(Colors.Green); // 将进度条前景色改为绿色
            }

            // MEMORY
            progressBar_mem.Value = meminfo;
            progressBar_mem.ToolTip = "内存占用" + progressBar_mem.Value + "%";
            if (progressBar_mem.Value > WN)
            {
                progressBar_mem.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            {
                progressBar_mem.Foreground = new SolidColorBrush(Colors.Green);
            }

            // DISK根目录
            progressBar_disk.Value = diskGeninfo;
            progressBar_disk.ToolTip = "根目录占用" + progressBar_disk.Value + "%";
            if (progressBar_disk.Value > WN)
            {
                progressBar_disk.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            {
                progressBar_disk.Foreground = new SolidColorBrush(Colors.Green);
            }

            // DISK数据盘
            progressBar_diskData.Value = diskDatainfo;
            progressBar_diskData.ToolTip = "数据盘占用" + progressBar_diskData.Value + "%";
            if (progressBar_diskData.Value > WN)
            {
                progressBar_diskData.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            {
                progressBar_diskData.Foreground = new SolidColorBrush(Colors.Green);
            }
        }


        // 开始监控服务器
        private void Is_Servers_Monitor(object sender, RoutedEventArgs e)
        {
            if (TB_FilePath.Text == "")     // 判断是否已导入Excel
            {
                MessageBox.Show("服务器信息未导入!");
                return;
            }
            list_1.IsEnabled = false;
            TB_FilePath.IsEnabled = false;
            TB_Command.IsEnabled = false;
            TB_2.Text = "正在轮巡服务器......";
            Servers_Inspection();
        }

        // 停止关闭监控服务器
        private void Un_Servers_Monitor(object sender, RoutedEventArgs e)
        {
            list_1.IsEnabled = true;
            TB_FilePath.IsEnabled = true;
            TB_Command.IsEnabled = true;
            BTN_Import.IsEnabled = true;
            TB_2.Text = "";
        }

        // 轮巡功能模块
        private void Servers_Inspection()
        {
            string filePath = "Inspection.txt";
            double WN = Convert.ToInt16(TB_WN.Text);   // 获取预警值
            List<Info> ls = new List<Info>(list_1.Items.Cast<Info>().ToList());  // 获取列表list_1内数据
            foreach (Info item in ls)   // 遍历列表数据行
            {
                if (CB_Monitor.IsChecked != true)   //  如果取消勾选巡检框就停止巡检
                {
                    return;
                }
                // 创建SSH连接并执行命令
                string coomand = TB_Command.Text;
                string[] outp = InitSSHConn(item.IP, Convert.ToInt16(item.Port), item.User, item.Password, coomand);
                double cpu = 100 - Convert.ToDouble(outp[0]); // cpu占用
                double mem = (Convert.ToDouble(outp[1]) / Convert.ToDouble(outp[2])) * 100;  // 内存占用
                double diskGen = 0;    // 根目录占用
                double diskData = 0;   // 数据盘占用
                if (outp[3] != "")  // 判断根目录是否查询到
                {
                    diskGen = Convert.ToDouble(outp[3]);
                }
                if (outp[4] != "")   // 判断是否有数据盘
                {
                    diskData = Convert.ToDouble(outp[4]);    // 数据盘占用
                }
                else if (outp[5] != "")   // 判断数据盘
                {
                    diskData = Convert.ToDouble(outp[5]);    // 数据盘占用
                }
                string content = item.IP + " " + "CPU:" + cpu + "%  " + "内存:" + mem + "%  " + "根目录:" + diskGen + "%  " + "数据盘:" + diskData + "%  " + "防火墙状态:" + outp[5];   // 输出数据
                if (cpu > WN || mem > WN || diskGen > WN || diskData > WN)      // 判断服务器指标是否达预警值
                {
                    content = "/////告警/////" + " " + content;     // 写入警告标记
                }
                string file_out = content + "\n";   // 追加进输出结果并换行
                File.AppendAllText(filePath, file_out);     // 追加写入TXT文件
                Thread.Sleep(1000);     // 暂停1秒防止SSH频繁访问
            }
            MessageBox.Show("巡检已结束，巡检结果已导入 Inspection.txt");    // 弹窗提示巡检完成
        }

        private void BTN_Out_Click(object sender, RoutedEventArgs e)
        {
            // 创建一个新的Excel文件
            using (ExcelPackage package = new ExcelPackage())
            {
                // 添加一个工作表
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Sheet1");

                // 写入数据
                worksheet.Cells[1, 1].Value = "192.168.1.1";
                worksheet.Cells[1, 2].Value = 22;
                worksheet.Cells[1, 3].Value = "root";
                worksheet.Cells[1, 4].Value = "passwd";

                // 保存到文件
                FileInfo file = new FileInfo("Import.xlsx");
                package.SaveAs(file);
                MessageBox.Show("Excel模板已下载至本程序所在目录下Import.xlsx");    // 弹窗提示下载完成
            }
        }

        // 勾选查询服务器性能框和防火墙状态框时禁用命令框，反之亦然
        private void CB_1_Checked(object sender, RoutedEventArgs e)
        {
            if ((CB_1.IsChecked ?? false) || (CB_ufw.IsChecked ?? false))
            {
                TB_Command.IsEnabled = false;
            }
        }

        private void CB_1_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!((CB_1.IsChecked ?? false) || (CB_ufw.IsChecked ?? false)))
            {
                TB_Command.IsEnabled = true;
            }
        }

        // 勾选防火墙状态框和查询服务器性能框时禁用命令框，反之亦然
        private void CB_ufw_Checked(object sender, RoutedEventArgs e)
        {
            if ((CB_1.IsChecked ?? false) || (CB_ufw.IsChecked ?? false))
            {
                TB_Command.IsEnabled = false;
            }
        }

        private void CB_ufw_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!((CB_1.IsChecked ?? false) || (CB_ufw.IsChecked ?? false)))
            {
                TB_Command.IsEnabled = true;
            }
        }

        // 警告框失焦时验证内容是否合法
        private void TB_WN_LostFocus(object sender, RoutedEventArgs e)
        {
            int reg = Convert.ToInt16(TB_WN.Text);
            if (!((reg > 0) && (reg < 100)))    
            {
                MessageBox.Show("请输入1~99的数");   // 提示
                TB_WN.Text = "80";   // 修改为默认值
            }
        }
    }
}
